#include <stdio.h>


int min(int a, int b){
    if (a > b)
        return b;
    return a;
}



int main() {
    int INF = 9999999;  // infinity
    int n = 5;
    int matrix[5][5] = {
        {0, 10, INF, 5, INF},
        {10, 0, 5, 5, 10},
        {INF, 5, 0 , INF, INF},
        {5, 5, INF, 0, 20},
        {INF, 10, INF, 20, 0}
    };


    // floyd warshall
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = min(matrix[i][j], matrix[i][k] + matrix[k][j]);
            }
        }
    }


    // print table
    for (int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            printf("%*d ",5, matrix[i][j]);
        }
        printf("\n");
    }
    
}