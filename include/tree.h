#ifndef TREE_H
#define TREE_H


#include "data.h"


int aralikta_olmayan_eleman_sayisi(TreeNode *root, int min, int max);
TreeNode *create_BST(int arr[], int length);
TreeNode *create_BT(int arr[], int length);                 // create binary tree from int array[]
int size_of_BT(TreeNode *root);
int get_level(int length);                              // calculate level of full and complated binary trees using number of items
int get_level_of(TreeNode *root);                           // calculate level of binary tree
int get_root_index(int length);                         // return root index as a full or complate binary tree
void print_tree(TreeNode *root);                            // print binary tree level order
void print_tree2(TreeNode *root, int tmp, int level);
void print_level(TreeNode *root, int level, int tmp);
TreeNode *root_sil(TreeNode *tree);



#endif